﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace PA.Http
{
    public class HttpServer : IHttpServer
    {
        private readonly TcpListener _listener;
        private bool _isAlive;
        private readonly Thread _newConnectionThread;

        private readonly ManualResetEvent _clientConnected = new ManualResetEvent(false);

        public HttpServer(int port)
        {
            _listener = new TcpListener(IPAddress.Any, port);
            _newConnectionThread = new Thread(ListenForNewConnections);
        }

        public void Start()
        {
            _isAlive = true;
            _newConnectionThread.Start();
        }

        public void Stop()
        {
            _isAlive = false;
        }

        private void ListenForNewConnections()
        {
            try
            {
                _listener.Start();
            }
            catch (Exception)
            {
                Console.WriteLine("An error occoured when starting the Http Server. Please check the chosen port is avaliiable.");
            }

            while (_isAlive)
            {
                _clientConnected.Reset();
                _listener.BeginAcceptTcpClient(AcceptTcpClient, _listener);
                _clientConnected.WaitOne();
            }
        }

        private void AcceptTcpClient(IAsyncResult ar)
        {
            var listener = (TcpListener) ar.AsyncState;
            var client = listener.EndAcceptTcpClient(ar);

            var  clientThread = new Thread(HandleNewConnection);
            clientThread.IsBackground = true;
            clientThread.Start(client);

            _clientConnected.Set();
        }

        private void HandleNewConnection(object clientObj)
        {
            // Get the http request from TcpClient
            var client = (TcpClient) clientObj;
            var inputStream = new BufferedStream(client.GetStream());
            var request = StreamReadLine(inputStream);

            var httpRequest = request.Split(' ');
            if (httpRequest.Length != 3)
                throw new Exception("Invalid Http Request");

            // Get http headers
            var headers = new Dictionary<string, string>();
            string headerLine;

            while ((headerLine = StreamReadLine(inputStream)) != null)
            {
                // Return if no more headers
                if (headerLine == "")
                    break;

                var seperator = headerLine.IndexOf(':');
                if(seperator == -1)
                    throw new Exception("Invalid Http header line: " + headerLine);

                var name = headerLine.Substring(0, seperator);
                var value = headerLine.Substring(seperator + 1, headerLine.Length - seperator - 1);

                headers.Add(name, value.Trim());
            }

            var paramenters = new Dictionary<string, string>();

            if (httpRequest[0] == "GET")
            {
                var querySeperator = httpRequest[1].IndexOf('?');

                if (querySeperator != -1)
                {
                    
                }
            }
            else if (httpRequest[0] == "POST")
            {
                
            }

        }

        private string StreamReadLine(Stream inputStream)
        {
            var data = "";
            while (true)
            {
                var nextChar = inputStream.ReadByte();
                if (nextChar == '\n') { break; }
                if (nextChar == '\r') { continue; }
                if (nextChar == -1) { Thread.Sleep(1); continue; };
                data += Convert.ToChar(nextChar);
            }
            return data;
        }
    }
}
