﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PA.Http
{
    public interface IHttpServer
    {
        void Start();
        void Stop();
    }
}
