﻿using System;
using PA.Http;

namespace ProjectAlpha
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Project Alpha.");
            Console.WriteLine("Type 'exit' in console to stop");

            var runner = new Runner();
            runner.Start();

            while (true)
            {
                var line = Console.ReadLine();

                if (line != null && line.Equals("exit"))
                    break;
            }

            runner.Stop();
        }
    }
}
