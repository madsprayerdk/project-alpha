﻿using PA.Http;
using ProjectAlpha.Properties;

namespace ProjectAlpha
{
    public class Runner
    {
        private readonly IHttpServer _httpServer;

        public Runner()
        {
            _httpServer = new HttpServer(Settings.Default.Port);
        }

        public void Start()
        {
            _httpServer.Start();
        }

        public void Stop()
        {
            _httpServer.Stop();
        }
    }
}
